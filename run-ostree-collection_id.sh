#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :
#

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)

. "${TESTDIR}/common/common.sh"

COLLECTION_ID="org.apertis.os"

test_collection_id_config()
{
    # Check if we have collection-id configured
    local collection_id=$(ostree config --group 'remote "origin"' get collection-id)
    [ "${collection_id}" = "${COLLECTION_ID}" ] || return 1
    
    # Additionally check if we have a collection ref
    local ref="$(ostree refs -c ${COLLECTION_ID} | tr -d '(),' | cut -f 2 -d ' ')"
    [ -n "${ref}" ] || return 1

    return 0
}

test_collection_id_booted_commit()
{
    # Get booted commit ID
    local boot_id="$(sudo ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"
    # See APERTIS-5745
    [ -n "${boot_id}" ] || return 1

    local collection_id=$(ostree show ${boot_id} --print-metadata-key ostree.collection-binding | tr -d "'")
    [ "${collection_id}" = "${COLLECTION_ID}" ] || return 1

    ostree show ${boot_id} --print-metadata-key ostree.ref-binding || return 1

    return 0
}

trap "test_failure" EXIT

src_test_pass <<-EOF
test_collection_id_config
test_collection_id_booted_commit
EOF

test_success
